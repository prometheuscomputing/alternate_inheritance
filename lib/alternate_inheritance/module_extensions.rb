class Module
  def extended_modules
    singleton_class = (class << self; self; end)
    singleton_class.included_modules
  end
end