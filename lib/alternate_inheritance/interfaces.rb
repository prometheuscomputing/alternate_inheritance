module AlternateInheritance::Interface
  include AlternateInheritance
  extend self
  attr_accessor :imm_implementors

  def extended(extender)
    extender.instance_variable_set("@abstract", true)
    extender.instance_variable_set("@interface", true)
    if extender.extended_modules.include?(AlternateInheritance)
      # This class or a ruby parent already extends AlternateInheritance, so only run hooks
      AlternateInheritance.extended(extender)
    else # Extend AlternateInheritance
      extender.extend(AlternateInheritance)
    end
    extender.imm_implementors ||= []
    # Tell any pre-existing children to extend AlternateInheritance::Interface
    extender.imm_children.each{|c| c.send(:extend, self)}
  end
  
  def inherited(child_class)
    super
    AlternateInheritance::Interface.extended(child_class)
  end

  # This probably won't be needed/used but including it anyway for completeness
  def implemented_by(*implementing_class)
    implementing_class.is_a?(Array) ? implementing_class.each{|i| _implemented_by(i)} : _child_of(implementing_class)
  end
  
  # Check if the current interface implements the passed class or classes
  def implemented_by?(*cls)
    return false unless cls.any?
    cls.inject(true){|is_implemented_by, c| is_implemented_by && _implemented_by?(c)}
  end
  
  def implementors(options = {})
    options = options.merge(:tree => :implementors)
    AlternateInheritance.traverse(self, ['children+self', 'immediate_implementors', 'children+self'], options)
  end
  
  def immediate_implementors
    @imm_implementors
  end
  
  def _implemented_by(implementing_class)
    # Ensure that AlternateInheritance is applied to parent_class
    unless implementing_class.extended_modules.include?(AlternateInheritance::InterfaceImplementor)
      raise "#{implementing_class} does not extend AlternateInheritance::InterfaceImplementor"
    end
    implementing_class._implements(self)
  end
  
  def _implemented_by?(cls)
    implementors.include?(cls)
  end
end

module AlternateInheritance::InterfaceImplementor
  include AlternateInheritance
  extend self
  attr_accessor :imm_interfaces

  def extended(extender)
    if extender.extended_modules.include?(AlternateInheritance)
      # This class or a ruby parent already extends AlternateInheritance, so only run hooks
      AlternateInheritance.extended(extender)
    else # Extend AlternateInheritance
      extender.extend(AlternateInheritance)
    end
    extender.imm_interfaces ||= []
    # Tell any pre-existing children to extend AlternateInheritance::InterfaceImplementor
    extender.imm_children.each{|c| c.send(:extend, self)}
  end
  
  # def included(includer)
  #   puts "#{includer} included #{self}"
  # end
  
  def inherited(child_class)
    AlternateInheritance::InterfaceImplementor.extended(child_class)
    super
  end
  
  def implements(*interface_classes)
    interface_classes.each{|i| _implements(i)}
  end
  
  # Check if the current class implements the passed interface or interfaces
  def implements?(*interfaces)
    return false unless interfaces.any?
    interfaces.inject(true){|does_implement, i| does_implement && _implements?(i)}
  end

  def interfaces(options = {})
    options = options.merge(:tree => :interfaces)
    AlternateInheritance.traverse(self, ['parents+self', 'immediate_interfaces', 'parents+self'], options)
  end
  
  def immediate_interfaces
    @imm_interfaces
  end
  
  def _implements(interface_class)
    # Ensure that AlternateInheritance is applied to parent_class
    unless interface_class.extended_modules.include?(AlternateInheritance::Interface)
      raise "#{interface_class} does not extend AlternateInheritance::Interface"
    end
    self.imm_interfaces << interface_class
    interface_class.imm_implementors << self
    
    # Delete 'interfaces' caches for this class and its children
    ([self] + children(:ignore_cache => true)).each do |k|
      k.delete_traversal_cache_for(:interfaces) if k.respond_to?(:delete_traversal_cache_for)
    end
    # Delete 'implementors' cache for the interface and for interface parents
    ([interface_class] + interface_class.parents(:ignore_cache => true)).each do |i|
      i.delete_traversal_cache_for(:implementors) if i.respond_to?(:delete_traversal_cache_for)
    end
    
    self.on_interface_addition(interface_class) if self.respond_to?(:on_interface_addition)
    interface_class.on_implement(self) if interface_class.respond_to?(:on_implement)
  end
  
  def _implements?(interface)
    interfaces.include?(interface)
  end
  
  def on_implement(implementor)
  end
end