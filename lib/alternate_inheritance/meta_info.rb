module AlternateInheritance
  
  # For more information about meta_info.rb, please see project Foundation, lib/Foundation/meta_info.rb
  
  # SUGGESTION: Treat "Optional" as meaning "can be nil", and define all constants, even if the value happens to be nil.
  
  # Required String
  GEM_NAME = "alternate_inheritance"
  # Required String
  VERSION = '2.2.0'
  # Optional String or Array of Strings
  AUTHORS = ["Sam Dana"]
  # Optional String or Array of Strings
  EMAILS = ["s.dana@prometheuscomputing.com"]
  # Optional String
  HOMEPAGE = nil
  # Required String
  SUMMARY = %q{Provides an alternate inheritance scheme}
  # Optional String
  DESCRIPTION = %q{Provides an alternative inheritance structure to the Ruby provided one. This project does _NOT_ copy superclass attributes and methods to the subclasses,
      but instead provides a framework to do so via hooks. Support for multiple inheritance and interfaces is also added.}
  
  # Required Symbol
  # This specifies the language the project is written in (not including the version, which is in LANGUAGE_VERSION).
  # A project should only have one language (not including, for example DSLs such as templating languages).
  # If a project has more than one language (not including DSLs), it should be split.
  # TEMPORARY EXCEPTION: see :frankenstein choice below.
  # The reason for a single language is that mixing up languages in one project complicates packaging, deployment, metrics, directory structure, and many other aspects of development.
  # Choices are currently:
  #   * :ruby (implies packaging as gem - contains ZERO java code)
  #   * :java (implies packaging as jar, ear, war, sar, etc (depending on TYPE) - contains ZERO ruby code, with exception of meta_info.rb)
  #   * :frankenstein (implies packaging as gem - contains BOTH ruby and java code - will probably deprecate this in favor of two separate projects)
  LANGUAGE = :ruby
  # This differs from Runtime version - this specifies the version of the syntax of LANGUAGE
  LANGUAGE_VERSION = ['> 1.8.1']
  # This is different from aGem::Specification.platform, which appears to be concerned with OS.
  # This defines which implentation of Ruby, Java, etc can be used.
  # Required Hash, in same format as DEPENDENCIES_RUBY.
  # The version part is used by required_ruby_version
  # Allowable keys depend on LANGUAGE. They are in VALID_<language.upcase>_RUNTIMES
  RUNTIME_VERSIONS = {
    :mri => ['>= 1.9.2'],
    :jruby => ['> 1.5.0']
  }
  # Required Symbol
  # Choices are currently:
  #   * :library - reusable functionality, not intended to stand alone
  #   * :utility - intended for use on command line
  #   * :web_app - an application that uses a web browser for it's GUI
  #   * :service - listens on some port. May include command line tools to manage the server.
  #   * :gui - has a Swing, Fox, WXwidget, etc GUI
  TYPE = :library
  # Required when TYPE is not :utility or :library.
  # Specifies what to invoke when the user double clicks on a packaged application.
  # Note also ".platypus" alternative.
  # This is a String path, relative to either bin or lib. If present in both, bin is used.
  # If in bin, this will be invoked through the operating system.
  LAUNCHER = nil
  
  # Optional Hashes - if nil, presumed to be empty.
  # There may be additional dependencies, for platforms (such as :maglev) other than :mri and :jruby
  # In the case of JRuby platform Ruby code that depends on a third party Java jar, where do we specify that?
  
  # Trying to install this under Ruby 1.8.7 I get:
  #   Error installing MM-0.0.6.gem:
  #   simplecov requires multi_json (~> 1.0.3, runtime)
  # So I have commented out some dependencies.
  # FIX: these dependency collections need to be specific to a LANGUAGE_VERSION. Maybe RUNTIME_VERSIONS as well.
  #      We also need :simplecov => nil, but only on Ruby > 1.8 }
  DEPENDENCIES_RUBY = { } # Add SCT here? (:sequel_change_tracker => 1.6.0)
  DEPENDENCIES_MRI = { }
  DEPENDENCIES_JRUBY = { }
  DEVELOPMENT_DEPENDENCIES_RUBY = {:rspec => nil } # test-unit is reccomended but not required (color codes Test::Unit test results)
  DEVELOPMENT_DEPENDENCIES_MRI = { }
  DEVELOPMENT_DEPENDENCIES_JRUBY = { }
  
  # An Array of strings that YARD will interpret as regular expressions of files to be excluded.
  YARD_EXCLUDE = ['lib/.*/templates/.*']
end