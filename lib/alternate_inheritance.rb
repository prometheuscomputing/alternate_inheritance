require 'alternate_inheritance/meta_info'
require 'alternate_inheritance/module_extensions'
require 'alternate_inheritance/interfaces'
# To include interface support, use the following in your code after requiring alternate_inheritance:
# require 'alternate_inheritance/interfaces'

module AlternateInheritance
  extend self
  # The immediate superclasses of the extending class (not including Ruby superclass)
  attr_accessor :imm_parents
  # The immediate children of the extending class (not including Ruby descendants)
  attr_accessor :imm_children
  # The immediate ruby descendants of this class
  attr_accessor :imm_ruby_children
  
  # The traversal_cache for this class
  attr_accessor :traversal_cache
  
  # Whether or not to include ruby inheritance in inheritance tracking methods
  attr_accessor :extend_ruby_inheritance
  # Extend ruby inheritance by default
  @extend_ruby_inheritance = true
  
  # Whether or not to initialize newly extending classes with current ruby children or just an empty arry
  # There is a performance hit when using this option, but only during model initialization.
  attr_accessor :populate_ruby_children_on_initialization
  
  # This variable determines where to stop returning "parents".
  # All constants above and including this variable will not be returned.
  attr_accessor :top_level_ruby_const_exclusive
  # Set Object to top_level_ruby_const_exclusive as default
  @top_level_ruby_const_exclusive = Object

  # Ruby hook that is run when this module is extended
  def extended(extender)
    extender.imm_children ||= []
    extender.imm_parents ||= []
    if AlternateInheritance.populate_ruby_children_on_initialization
      extender.imm_ruby_children ||= ObjectSpace.each_object(::Class).select { |klass| klass.superclass == self }
    else
      extender.imm_ruby_children ||= []
    end
    extender.traversal_cache ||= {}
    # Add interface implementor functionality unless extender already does so or is an interface
    extender.extend(AlternateInheritance::InterfaceImplementor) unless (extender.extended_modules & [AlternateInheritance::Interface, AlternateInheritance::InterfaceImplementor]).any?
  end
  
  # Add the current class as a child of the given parent class
  def child_of(*parent_classes)
    parent_classes.each{|p| _child_of(p)}
  end
  
  # This probably won't be needed/used but including it anyway for completeness
  def parent_of(*child_class)
    child_class.is_a?(Array) ? child_class.each{|c| _parent_of(c)} : _parent_of(child_class)
  end
  
  # Check if the current class inherits from the passed class or classes
  def inherits_from?(*cls)
    return false unless cls.any?
    cls.inject(true){|does_inherit, c| does_inherit && _inherits_from?(c)}
  end
  
  # Retrieve a list of parents, sorted alphabetically by default
  def parents(options = {})
    options = options.merge(:tree => :parents)
    AlternateInheritance.traverse(self, 'parents', options)
  end
  
  # Retrieve a list of children, sorted alphabetically by default
  def children(options = {})
    options = options.merge(:tree => :children)
    AlternateInheritance.traverse(self, 'children', options)
  end
  
  # Get all ruby classes inheriting from this class
  # def ruby_children
  #   puts "Warning: use of this method may cause severe performace degradation"
  #   ObjectSpace.each_object(::Class).select { |klass| klass < self }
  # end
  
  # Get only the Ruby classes that list this class as their direct superclass
  def immediate_ruby_children
    imm_ruby_children
  end
  
  # Get all tracked immediate children as well as the immediate Ruby descendants of this class
  def immediate_children
    irc = AlternateInheritance.extend_ruby_inheritance ? immediate_ruby_children : []
    (imm_children + irc).uniq
  end
  
  def has_children?
    immediate_children.any?
  end
  
  # Get all tracked immediate parents as well as the direct ruby superclass of this class
  def immediate_parents
    irp = (AlternateInheritance.extend_ruby_inheritance && immediate_ruby_parent) ? [immediate_ruby_parent] : []
    (imm_parents + irp).uniq
  end
  
  # Get all ruby superclasses for this class
  def ruby_parents
    tlrc = AlternateInheritance.top_level_ruby_const_exclusive
    tlrc_index = superclasses.index(tlrc) if tlrc
    return [] if tlrc_index == 0
    last_index = tlrc_index ? tlrc_index - 1 : -1
    superclasses[0..last_index]
  end
  
  def superclasses
    return [] if self.class == Module # No superclasses for modules
    # Ruby won't allow this to change, so go ahead and store permanently
    @superclasses ||=
        begin
          sup_classes = []
          current_class = self.superclass
          while current_class
            sup_classes << current_class
            current_class = current_class.superclass
          end
          sup_classes
        end
  end
  
  # Get the superclass of this class
  # This is not the same as Ruby's #superclass method, since it will return nil if the class is at the "top level"
  def immediate_ruby_parent
    ruby_parents[0]
  end
  
  def delete_traversal_cache_for(cache_name)
    self.traversal_cache.delete_if{|k,v| k.is_a?(Hash) ? k[:tree] == cache_name : k == cache_name}
  end
  
  def traverse(object, relations, opts = {})
    options = {:traversal => :alphabetized}.merge(opts)
    ignore_cache = options.delete(:ignore_cache)
    cache = object.traversal_cache[options] unless ignore_cache
    return cache.dup if cache
    current_level = [object]
    relations = Array(relations)
    relations.each do |relation|
      last_level = current_level
      include_self = relation.slice!(/\+self$/)
      # include_self = true
      if relation =~ /^immediate/
        current_level = current_level.collect {|obj| obj.send(relation) if obj.respond_to?(relation)}.flatten.compact.uniq
      else
        current_level = current_level.collect do |obj| 
          level = []
          level << obj if include_self
          level += _traverse(obj, relation, options.merge(:ignore_cache => ignore_cache))
        end.flatten.uniq
      end
    end
    current_level.sort_by!{|e| e.to_s} if options[:traversal] == :alphabetized
    object.traversal_cache[options] = current_level
    current_level.dup
  end
  
  # Iterate over the given relationship using the given traversal method and options
  # Relies heavily on variable naming conventions within this project
  def _traverse(object, relation_name, options = {})
    method_name = relation_name.to_sym
    immediate_method_name = "immediate_#{relation_name}".to_sym
    found_relations = []
    case options[:traversal]
    when :alphabetized, :breadth
      current_level = object.send(immediate_method_name)
      while current_level.any?
        # Add the current depth to the list of found parents
        found_relations += current_level
        # TODO: the following lines for this traversal method are unnecessarily run on the last iteration (found_relations is the returned result)
        # Get the next depth and assign to current_level
        current_level = current_level.collect{|r| r.respond_to?(immediate_method_name) ? r.send(immediate_method_name) : []}.flatten
        # Remove any relations that appear multiple times
        current_level.uniq!
        # Reject any relations already found
        current_level.reject!{|r| found_relations.include?(r)}
      end
    when :depth
      immediate_relations = object.send(immediate_method_name)
      immediate_relations.each do |r|
        found_relations << r
        # Recurse on this child
        found_relations += r.respond_to?(immediate_method_name) ? r.send(method_name, options) : []
      end
    else # initial implementation (fastest?)
      immediate_relations = object.send(immediate_method_name)
      found_relations = (immediate_relations + immediate_relations.collect{|r| r.respond_to?(immediate_method_name) ? r.send(method_name, options) : [] })
      found_relations = found_relations.flatten.uniq
    end
    found_relations
  end
  
  def _parent_of(child_class)
    unless child_class.extended_modules.include?(AlternateInheritance)
      raise "#{child_class} does not extend AlternateInheritance"
    end
    child_class._child_of(self)
  end
  
  def _child_of(parent_class)
    # Ensure that AlternateInheritance is applied to parent_class
    unless parent_class.extended_modules.include?(AlternateInheritance)
      raise "#{parent_class} does not extend AlternateInheritance"
    end
    self.imm_parents << parent_class
    parent_class.imm_children << self
    
    # extend AlternateInheritance::InterfaceImplementor and/or AlternateInheritance::Interface as appropriate
    self.extend(AlternateInheritance::InterfaceImplementor) if parent_class.extended_modules.include?(AlternateInheritance::InterfaceImplementor) && !self.extended_modules.include?(AlternateInheritance::InterfaceImplementor)
    self.extend(AlternateInheritance::Interface) if parent_class.extended_modules.include?(AlternateInheritance::Interface) && !self.extended_modules.include?(AlternateInheritance::Interface)
    
    # Delete 'parents' caches for this class and its children
    ([self] + children(:ignore_cache => true)).each do |k|
      k.delete_traversal_cache_for(:parents) if k.respond_to?(:delete_traversal_cache_for)
    end
    # Delete 'children' cache for this class and its parents
    ([self] + parents(:ignore_cache => true)).each do |k|
      k.delete_traversal_cache_for(:children) if k.respond_to?(:delete_traversal_cache_for)
    end
    
    self.on_parent_addition(parent_class) if self.respond_to?(:on_parent_addition)
    parent_class.on_inherit(self) if parent_class.respond_to?(:on_inherit)
  end
  
  # Hook ruby inheritance
  def inherited(child_class)
    @imm_ruby_children << child_class if child_class.superclass == self
    
    # Run initial setup code (module is already extended since we are subclassing an extended class)
    if child_class.extended_modules.include?(AlternateInheritance)
      # This class or a parent already extends AlternateInheritance, so only run hooks
      AlternateInheritance.extended(child_class)
    else # Extend AlternateInheritance
      child_class.extend(AlternateInheritance)
    end
    
    # Delete 'parents' caches for this class and its children
    ([child_class] + child_class.children(:ignore_cache => true)).each do |k|
      k.delete_traversal_cache_for(:parents) if k.extended_modules.include?(AlternateInheritance)
    end
    # Delete 'children' cache for this class and its parents
    ([child_class] + child_class.parents(:ignore_cache => true)).each do |k|
      k.delete_traversal_cache_for(:children) if k.extended_modules.include?(AlternateInheritance)
    end
    
    super
    on_inherit(child_class) if self.respond_to?(:on_inherit)
    child_class.on_parent_addition(self) if child_class.respond_to?(:on_parent_addition)
  end
  
  def _inherits_from?(cls)
    parents.include?(cls)
  end
  
  def on_parent_addition(parent_class)
  end
  
  def on_inherit(child_class)
  end
  
  def on_interface_addition(interface)
  end
end
