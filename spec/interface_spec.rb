require 'spec_helper'

describe AlternateInheritance::Interface do
  it "should keep track of the interfaces implemented by a class" do
    # Alphabetized
    expect(Parent.interfaces).to be == [HasName]
    expect(AnotherParent.interfaces).to be == [HasName]
    expect(Child.interfaces).to be == [HasName]
    expect(NonRelation.interfaces).to be_empty # Does not inherit from Parent or AnotherParent
    expect(GrandChild.interfaces).to be == [HasName, PlaysWithToys]
    expect(SecondChild.interfaces).to include(HasName) # TODO: prevent from being modified by hook_spec
    expect(GreatGrandChild.interfaces).to contain_exactly(GrandChildInterface, ChildInterface, HasName, PlaysWithToys)
    
    # Breadth first
    expect(Parent.interfaces(:traversal => :breadth)).to include(HasName)
    expect(AnotherParent.interfaces(:traversal => :breadth)).to include(HasName)
    expect(Child.interfaces(:traversal => :breadth)).to include(HasName)
    expect(NonRelation.interfaces(:traversal => :breadth)).to be_empty # Does not inherit from Parent or AnotherParent
    expect(GrandChild.interfaces(:traversal => :breadth)).to include(HasName, PlaysWithToys)
    expect(SecondChild.interfaces(:traversal => :breadth)).to include(HasName)
    expect(GreatGrandChild.interfaces(:traversal => :breadth)).to contain_exactly(GrandChildInterface, ChildInterface, HasName, PlaysWithToys)
    
    # Depth first
    expect(Parent.interfaces(:traversal => :depth)).to include(HasName)
    expect(AnotherParent.interfaces(:traversal => :depth)).to include(HasName)
    expect(Child.interfaces(:traversal => :depth)).to include(HasName)
    expect(NonRelation.interfaces(:traversal => :depth)).to be_empty # Does not inherit from Parent or AnotherParent
    expect(GrandChild.interfaces(:traversal => :depth)).to include(HasName, PlaysWithToys)
    expect(SecondChild.interfaces(:traversal => :depth)).to include(HasName)
    expect(GreatGrandChild.interfaces(:traversal => :depth)).to contain_exactly(GrandChildInterface, ChildInterface, HasName, PlaysWithToys)
  end
  
  it "should not break when getting the interfaces for a class that inherits from a class that does not implement AlternateInheritance" do
    expect(FunnyString.interfaces).to be == []
  end
    
  
  it "should keep track of the classes that implement an interface" do
    # Alphabetized
    expect(HasName.implementors).to be == [AnotherParent, Child, GrandChild, GreatGrandChild, Parent, RubyChild, SecondChild]
    expect(PlaysWithToys.implementors).to be == [GrandChild, GreatGrandChild, RubyChild]
    
    # Breadth first
    expect(HasName.implementors(:traversal => :breadth)).to include(Parent, AnotherParent, Child, GrandChild, GreatGrandChild, SecondChild)
    expect(PlaysWithToys.implementors(:traversal => :breadth)).to include(GrandChild, GreatGrandChild, RubyChild)
    
    # Depth first
    expect(HasName.implementors(:traversal => :depth)).to include(Parent, AnotherParent, Child, GrandChild, GreatGrandChild, SecondChild)
    expect(PlaysWithToys.implementors(:traversal => :depth)).to include(GrandChild, GreatGrandChild, RubyChild)
  end
  
  it "should be able to handle ruby inheritance when tracking interfaces/implementors" do
    expect(ChildInterface.implementors).to include(GreatGrandChild)
    expect(HasName.implementors).to include(RubyChild)
    expect(HasName.implementors).to include(GreatGrandChild)
    expect(PlaysWithToys.implementors).to include(RubyChild)
    expect(PlaysWithToys.implementors).to include(GreatGrandChild)    
    expect(RubyChild.interfaces).to include(HasName)
    expect(RubyChild.interfaces).to include(PlaysWithToys)

    # The above interfaces method likely also returns ChildInterface, but that depends on the order that specs are run.
  end
  
  it "should allow interfaces to have children/parents" do
    # Alphabetized
    expect(ChildInterface.parents).to be == [HasName]
    expect(HasName.children).to be == [ChildInterface, GrandChildInterface]
    
    # Breadth first
    expect(ChildInterface.parents(:traversal => :breadth)).to include(HasName)
    expect(HasName.children(:traversal => :breadth)).to include(ChildInterface)
    
    # Depth first
    expect(ChildInterface.parents(:traversal => :depth)).to include(HasName)
    expect(HasName.children(:traversal => :depth)).to include(ChildInterface)
  end
  
  it "should be able to query the interfaces implemented by a class" do
    expect(Parent.implements?(HasName)).to be true
    expect(Parent.implements?(ChildInterface)).to be false
    expect(GrandChild.implements?(HasName, PlaysWithToys)).to be true
  end
  
  it "should be able to query the classes that implement an interface" do
    expect(HasName.implemented_by?(SecondChild, Parent, AnotherParent, Child, GrandChild)).to be true
    expect(PlaysWithToys.implemented_by?(GrandChild)).to be true
  end
  
  it "should raise an error when attempting to implement an interface that does not implement AlternateInheritance::Interface" do
    expect(lambda {Child.implements Untracked}).to raise_error(/Untracked does not extend AlternateInheritance::Interface/i)
  end

  it "should raise an error when attempting to specify an interface implemention for a class that does not implement AlternateInheritance::InterfaceImplementor" do
    expect(lambda {ChildInterface.implemented_by Untracked}).to raise_error(/Untracked does not extend AlternateInheritance::InterfaceImplementor/i)
  end
end