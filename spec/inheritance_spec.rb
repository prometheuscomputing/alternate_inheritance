require 'spec_helper'

describe AlternateInheritance do
  before(:all) do
    AlternateInheritance.top_level_ruby_const_exclusive = nil
    AlternateInheritance.extend_ruby_inheritance = false
  end
  after(:all) do
    AlternateInheritance.top_level_ruby_const_exclusive = Object
    AlternateInheritance.extend_ruby_inheritance = true
  end
  it "should keep track of parent classes" do
    
    # # Alphabetized (default)
    expect(Parent.parents).to be_empty
    expect(AnotherParent.parents).to be_empty
    expect(Child.parents).to include(Parent, AnotherParent)
    expect(NonRelation.parents).to be_empty
    # Check sorting order
    expect(GrandChild.parents).to be == [AnotherParent, Child, NonRelation, Parent]
    
    # Breadth first
    expect(Parent.parents(:traversal => :breadth)).to be_empty
    expect(AnotherParent.parents(:traversal => :breadth)).to be_empty
    expect(Child.parents(:traversal => :breadth)).to include(Parent, AnotherParent)
    expect(NonRelation.parents(:traversal => :breadth)).to be_empty
    gc_parents = GrandChild.parents(:traversal => :breadth)
    # Ensure that parents are ordered Breadth-First (not caring how each level is ordered)
    first_level = gc_parents.shift 2
    expect(first_level).to include(NonRelation, Child)
    second_level = gc_parents
    expect(second_level).to include(Parent, AnotherParent)
    
    # Depth first
    expect(Parent.parents(:traversal => :depth)).to be_empty
    expect(AnotherParent.parents(:traversal => :depth)).to be_empty
    expect(Child.parents(:traversal => :depth)).to include(Parent, AnotherParent)
    expect(NonRelation.parents(:traversal => :depth)).to be_empty
    gc_parents = GrandChild.parents(:traversal => :depth)
    # First check that elements are correct
    expect(gc_parents).to contain_exactly(NonRelation, Child, Parent, AnotherParent)
    # Check against possible valid depth-first traversals...
    expect((gc_parents == [NonRelation, Child, Parent, AnotherParent] || 
      gc_parents == [NonRelation, Child, AnotherParent, Parent] || 
      gc_parents == [Child, AnotherParent, Parent, NonRelation] || 
      gc_parents == [Child, Parent, AnotherParent, NonRelation])).to be true
  end
  
  it "should keep track of child classes" do
    # Alphabetized
    expect(Parent.children).to be == [Child, GrandChild, SecondChild]
    expect(AnotherParent.children).to be == [Child, GrandChild, SecondChild]
    expect(Child.children).to be == [GrandChild]
    expect(NonRelation.children).to be == [GrandChild]
    expect(GrandChild.children).to be_empty
    
    # Breadth first
    expect(Parent.children(:traversal => :breadth)).to include(Child, GrandChild, SecondChild)
    expect(AnotherParent.children(:traversal => :breadth)).to include(Child, GrandChild, SecondChild)
    expect(Child.children(:traversal => :breadth)).to include(GrandChild)
    expect(NonRelation.children(:traversal => :breadth)).to include(GrandChild)
    expect(GrandChild.children(:traversal => :breadth)).to be_empty
    
    # Depth first
    expect(Parent.children(:traversal => :depth)).to include(Child, GrandChild, SecondChild)
    expect(AnotherParent.children(:traversal => :depth)).to include(Child, GrandChild, SecondChild)
    expect(Child.children(:traversal => :depth)).to include(GrandChild)
    expect(NonRelation.children(:traversal => :depth)).to include(GrandChild)
    expect(GrandChild.children(:traversal => :depth)).to be_empty
  end
  
  it "should be able to list immediate children" do
    expect(Parent.immediate_children).to include(Child)
    expect(AnotherParent.immediate_children).to include(Child)
    expect(Child.immediate_children).to include(GrandChild)
    expect(NonRelation.immediate_children).to include(GrandChild)
    expect(GrandChild.immediate_children).to be_empty
  end
  
  it "should be able to list immediate parents" do
    expect(Parent.immediate_parents).to be_empty
    expect(AnotherParent.immediate_parents).to be_empty
    expect(Child.immediate_parents).to include(Parent, AnotherParent)
    expect(NonRelation.immediate_parents).to be_empty
    expect(GrandChild.immediate_parents).to include(Child, NonRelation)
  end
  
  it "should be able to query inheritance" do
    expect(GrandChild.inherits_from?(Parent)).to be true
    expect(GrandChild.inherits_from?(NonRelation, Child, Parent, AnotherParent)).to be true
  end
  
  it "should raise an error if an untracked class is used" do
    expect(lambda {Child.child_of Untracked}).to raise_error(/Untracked does not extend AlternateInheritance/i)
  end
  
  it "should not be super slow!" do
    before_time = Time.now
    puts "testing children"
    time_taken = time {
      10000.times do
        Parent.children
      end
    }
    expect(time_taken).to be < 1
    puts "Took: #{time_taken} seconds"
    #expect(time_taken).to be < 1
    before_time = Time.now
    puts "testing parents"
    time_taken = time {
      10000.times do
        GrandChild.parents
      end
    }
    puts "Took: #{time_taken} seconds"
    expect(time_taken).to be < 1
  end
  
  it "should be using cache!" do
    # Make sure cache is set
    Parent.children
    # Modify cache
    Parent.traversal_cache[{:traversal=>:alphabetized, :tree=>:children}] = [Hash]
    # Check for modified result
    expect(Parent.children).to be == [Hash]
    # Check ignore_cache option
    expect(Parent.children(:ignore_cache => true)).to be == [Child, GrandChild, SecondChild]
    # Clear bad cache
    Parent.delete_traversal_cache_for(:children)
    # Check that good value is recalculated
    expect(Parent.children).to be == [Child, GrandChild, SecondChild]
  end
end
