require 'spec_helper'

describe "Ruby inheritance extension" do
  it "should be able to include any inheritance defined by traditional ruby inheritance" do
    expect(Employee.parents).to contain_exactly(Person)
    expect(Employee.inherits_from?(Person)).to be true
    expect(Person.children).to contain_exactly(Employee, Salesman, CodeMonkey, Developer, Dummy, Mannequin, ClothesModel)
  end
  
  it "should be able to manage ruby defined inheritance alongside alternatively defined inheritance" do
    expect(Salesman.parents).to contain_exactly(Person, Seller)
    expect(Salesman.inherits_from?(Person, Seller)).to be true
    expect(FunnyString.parents).to contain_exactly(String, Funny)
  end
  
  it "should be able to manage ruby defined inheritance when defining prototype classes" do
    # PrototypeChild3 has nil for a name, and should be sorted first
    PrototypeParent.children == [$PrototypeChild3, PrototypeChild1, PrototypeChild2]
    expect(PrototypeChild1.parents).to be == [PrototypeParent]
    expect(PrototypeChild2.parents).to be == [PrototypeParent]
    expect($PrototypeChild3.parents).to be == [PrototypeParent]
  end
  
  it "should automatically extend AlternateInheritance for any class that inherits from an extended class" do
    expect(Developer.respond_to?(:parents)).to be true
    expect(Developer.parents).to contain_exactly(Person, Employee)
  end
  
  it "should be able to list immediate children" do
    expect(Person.immediate_children).to contain_exactly(Employee, Salesman, Dummy)
    expect(Seller.immediate_children).to contain_exactly(Salesman)
    expect(Employee.immediate_children).to contain_exactly(Developer)
    expect(Salesman.immediate_children).to be_empty
    expect(Developer.immediate_children).to contain_exactly(CodeMonkey)
    expect(CodeMonkey.immediate_children).to be_empty
    expect(Funny.immediate_children).to contain_exactly(FunnyString)
    expect(FunnyString.immediate_children).to be_empty
  end
  
  it "should be able to list immediate parents" do
    expect(Person.immediate_parents).to be_empty
    expect(Seller.immediate_parents).to be_empty
    expect(Employee.immediate_parents).to contain_exactly(Person)
    expect(Salesman.immediate_parents).to contain_exactly(Seller, Person)
    expect(Developer.immediate_parents).to contain_exactly(Employee)
    expect(CodeMonkey.immediate_parents).to contain_exactly(Developer)
    expect(Funny.immediate_parents).to be_empty
    expect(FunnyString.immediate_parents).to contain_exactly(String, Funny)
  end
  
  it "should allow mixing normal and alternate inheritance" do
    # Test Parents
    expect(ClothesModel.parents).to contain_exactly(Dummy, Person, Mannequin)
    expect(Mannequin.parents).to contain_exactly(Dummy, Person)
    expect(Dummy.parents).to contain_exactly(Person)
    expect(Person.parents).to contain_exactly()
    # Test Children
    expect(ClothesModel.children).to contain_exactly()
    expect(Mannequin.children).to contain_exactly(ClothesModel)
    expect(Dummy.children).to contain_exactly(Mannequin, ClothesModel)
    expect(Person.children).to contain_exactly(Employee, Salesman, CodeMonkey, Developer, Dummy, Mannequin, ClothesModel)
    expect(Funny.parents).to be_empty
    expect(FunnyString.parents).to contain_exactly(Funny, String)
  end
  
  it "should work with all traversal options when I have a parent that does not have AlternateInheritance" do
    expect(Funny.parents).to be_empty
    expect(FunnyString.parents).to be == [Funny, String]
    expect(Funny.parents(:traversal => :breadth)).to be_empty
    expect(FunnyString.parents(:traversal => :breadth)).to contain_exactly(Funny, String)
    expect(Funny.parents(:traversal => :depth)).to be_empty
    expect(FunnyString.parents(:traversal => :depth)).to contain_exactly(Funny, String)
    expect(Funny.parents(:traversal => :fast)).to be_empty
    expect(FunnyString.parents(:traversal => :fast)).to contain_exactly(Funny, String)
  end
  
  it "should execute the on_inherit hook when using ruby inheritance" do
    expect(Developer.last_child_class).to be == "CodeMonkey"
  end
  
  it "should not be super slow!" do
    before_time = Time.now
    puts "testing children"
    time_taken = time {
      10000.times do
        Person.children
      end
    }
    puts "Took: #{time_taken} seconds"
    expect(time_taken).to be < 1
    before_time = Time.now
    puts "testing parents"
    time_taken = time {
      10000.times do
        CodeMonkey.parents
      end
    }
    puts "Took: #{time_taken} seconds"
    expect(time_taken).to be < 1
  end
end