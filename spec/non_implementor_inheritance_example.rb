class Funny
  extend AlternateInheritance
end

class FunnyString < String
  extend AlternateInheritance
  child_of Funny
end