require 'spec_helper'
require 'hook_example'

describe "Inheritance and interface hooks" do
  it "should execute inheritance hooks when adding a parent-child relationship" do
    # Make NewParent another parent of NewChild
    NewChild.child_of NewParent
    # This will be true if hook executed...
    expect(NewChild.parent_class_var).to be == 'NewParent'

    # Make NewChild a parent of NewGrandChild
    NewGrandChild.child_of NewChild
    # This will be true if hook executed...
    expect(NewChild.child_class_var).to be == 'NewGrandChild'
  end
  
  it "should execute interface hooks when adding an interface-implementor relationship" do
    # Make NewChild implement ChildInterface
    NewChild.implements HookInterface
    expect(NewChild.interface_class_var).to be == 'HookInterface'
    expect(NewChild.second_interface_class_var).to be == 'HookInterface'
  end
  
  it "should be able to access class attributes in inheritance hooks" do
    expect(Chicken.result_of_on_inherit).not_to be_nil
    expect(Chicken.result_of_on_inherit).not_to be_empty
    immediate_interfaces = Chicken.result_of_on_inherit[0]
    immediate_parents = Chicken.result_of_on_inherit[1]
    immediate_ruby_children = Chicken.result_of_on_inherit[2]
    children = Chicken.result_of_on_inherit[3]
    parents = Chicken.result_of_on_inherit[4]
    ruby_parents = Chicken.result_of_on_inherit[5]
    interfaces = Chicken.result_of_on_inherit[6]
    expect(immediate_interfaces).to be == []
    expect(immediate_parents).to be == [Bird]
    expect(immediate_ruby_children).to be == []
    expect(children).to be == []
    expect(parents).to be == [Animal, Bird]
    expect(ruby_parents).to contain_exactly(Animal, Bird)
    expect(interfaces).to be == []
  end
end

