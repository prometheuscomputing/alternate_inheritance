class Person
  extend AlternateInheritance
end

class Seller
  extend AlternateInheritance
end

class Employee < Person
  extend AlternateInheritance
end

class Salesman < Person
  extend AlternateInheritance
  child_of Seller
end

class Developer < Employee
  class << self
    attr_accessor :last_child_class
  end
  def self.on_inherit(child_class)
    self.last_child_class = child_class.to_s
  end
end

class CodeMonkey < Developer
end

class Dummy
  extend AlternateInheritance
  child_of Person
end

class Mannequin < Dummy
  extend AlternateInheritance
end

class ClothesModel
  extend AlternateInheritance
  child_of Mannequin
end

class PrototypeParent
  extend AlternateInheritance
end
class PrototypeChild1 < PrototypeParent
end
PrototypeChild2 = Class.new(PrototypeParent)
$PrototypeChild3 = Class.new(PrototypeParent)