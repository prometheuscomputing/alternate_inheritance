class Parent
  extend AlternateInheritance
end

class AnotherParent
  extend AlternateInheritance
end

class Child
  extend AlternateInheritance
  child_of Parent
  child_of AnotherParent
end

class NonRelation
  extend AlternateInheritance
end

class GrandChild
  extend AlternateInheritance
  child_of Child
  child_of NonRelation
end

class Untracked
end