require 'interface_example'

class HookInterface
  extend AlternateInheritance::Interface
  def self.on_implement(implementing_class)
    implementing_class.second_interface_class_var = self.to_s if implementing_class.respond_to?(:second_interface_class_var)
  end
end

class NewChild
  extend AlternateInheritance::InterfaceImplementor
  class << self
    attr_accessor :parent_class_var
    attr_accessor :child_class_var
    attr_accessor :interface_class_var
    attr_accessor :second_interface_class_var
  end

  def self.on_parent_addition(parent_class)
    super
    self.parent_class_var = parent_class.to_s
  end
  
  def self.on_inherit(child_class)
    super
    self.child_class_var = child_class.to_s
  end
  
  def self.on_interface_addition(interface_class)
    super
    self.interface_class_var = interface_class.to_s
  end
end

class NewParent
  extend AlternateInheritance::InterfaceImplementor
end

class NewGrandChild
  extend AlternateInheritance::InterfaceImplementor
end

class Animal
  class << self
    attr_accessor :result_of_on_inherit
  end
  extend AlternateInheritance::InterfaceImplementor
  def self.on_inherit(child_class)
    child_class.result_of_on_inherit = [
      child_class.immediate_interfaces,
      child_class.immediate_parents,
      child_class.immediate_ruby_children,
      child_class.children,
      child_class.parents,
      child_class.ruby_parents,
      child_class.interfaces
    ]
  end
end

class Bird < Animal
end

class Chicken < Bird
end
  