require 'inheritance_example'

# make a couple interfaces
module HasName
  extend AlternateInheritance::Interface
end

module PlaysWithToys
  extend AlternateInheritance::Interface
end

# Assign implementors
class Parent
  extend AlternateInheritance::InterfaceImplementor
  implements HasName
end
class AnotherParent
  extend AlternateInheritance::InterfaceImplementor
  implements HasName
end

class GrandChild
  extend AlternateInheritance::InterfaceImplementor
  implements PlaysWithToys
end

# make a child interface
# Gets added programmatically to NewChild by hook_spec
class ChildInterface
  extend AlternateInheritance::Interface
  child_of HasName
end

class GrandChildInterface < ChildInterface
end

# Add more children to Parent and AnotherParent
class SecondChild
  extend AlternateInheritance::InterfaceImplementor
  child_of Parent
  child_of AnotherParent
end

class RubyChild < SecondChild
  # extend AlternateInheritance::InterfaceImplementor
  implements PlaysWithToys
end

# Test cache invalidation by caching implementors state without GreatGrandChild
HasName.implementors
class GreatGrandChild < GrandChild
  # extend AlternateInheritance::InterfaceImplementor
  implements GrandChildInterface
end